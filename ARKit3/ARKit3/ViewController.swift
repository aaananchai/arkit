//
//  ViewController.swift
//  ARKit3
//
//  Created by Ananchai Mankhong on 4/6/2563 BE.
//  Copyright © 2563 Ananchai Mankhong. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate{

    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    
    var statusMessage: String = ""
    var trackingStatus: String = ""
    var fishNode: SCNNode!
    var fishCount: Int = 5
    var fishStyle: Int = 0
    var fishOffset = SCNVector3(0.0, 0.1, 0.0)
    
    var focusPoint: CGPoint!
    var focusNode: SCNNode!
    var isStarted: Bool = false
    var isReseted: Bool = false
    var isFoundPlane: Bool = false
    var currentAngleY: Float = 0.0
    var localTranslatePosition: CGPoint = CGPoint(x: 0.0, y: 0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sceneView.delegate = self
        setupGestureRecognizer()
        self.loadModels()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        initARSession()
        initSceneView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    func initARSession() {
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        configuration.environmentTexturing = .automatic
        configuration.planeDetection = .horizontal
        // Run the view's session
        sceneView.session.run(configuration)
    }

    func initSceneView() {
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(orientationChaned),
//                                               name: UIDevice.orientationDidChangeNotification,
//                                               object: nil)
        sceneView.scene.physicsWorld.speed = 0.05
        sceneView.scene.physicsWorld.timeStep = 1.0 / 60.0
    }
    
    func loadModels() {
        setupFishNode()
        setupFocusNode()
        sceneView.autoenablesDefaultLighting = true
    }
    
    func setupFishNode() {
        let fishScene = SCNScene(named: "art.scnassets/Fish.scn")!
        fishNode = fishScene.rootNode.childNode(withName: "Fish", recursively: false)
    }
    
    func setupGestureRecognizer() {
        let swipeGestures = setupSwipeGestures()
        setupPanGestures(swipeGestures: swipeGestures)
        setupPinchGesture()
        setupTapGesture()
        setupLongPressGesture()
    }
    
    func setupFocusNode() {
        let focusScene = SCNScene(named: "art.scnassets/focus.scn")!
        focusNode = focusScene.rootNode.childNode(withName: "focus", recursively: false)!
        focusNode.geometry?.firstMaterial = focusNode.geometry?.material(named: "blue")
        sceneView.scene.rootNode.addChildNode(focusNode)
        if isReseted {
            sceneView.autoenablesDefaultLighting = true
            isReseted = false  
        }
    }
    
    func throwFishNode(transform: SCNMatrix4, offset: SCNVector3) {
        // Adding force
        let distance = simd_distance(focusNode.simdPosition,
                                     simd_make_float3(transform.m41,
                                                      transform.m42,
                                                      transform.m43))
        let direction = SCNVector3(-(distance * 2.5) * transform.m31,
                                   -(distance * 2.5) * (transform.m32 - Float.pi / 4),
                                   -(distance * 2.5) * transform.m33)
        let position = SCNVector3(transform.m41 + offset.x,
                                  transform.m42 + offset.y,
                                  transform.m43 + offset.z)
        fishNode.name = "Fish"
        fishNode.scale = SCNVector3(x: 0.05, y: 0.05, z: 0.05)
        fishNode.position = position
        fishNode.physicsBody = .dynamic()
        insertDirectionalLight(position: position)
        let rotation = SCNVector3(Double.random(min: 0, max: Double.pi),
                                  Double.random(min: 0, max: Double.pi),
                                  Double.random(min: 0, max: Double.pi))
        fishNode.eulerAngles = rotation
        fishNode.physicsBody?.resetTransform()
        fishNode.physicsBody?.applyForce(direction, asImpulse: true)
        // This fixed bug for the falling out of the plane
        if #available(iOS 12.0, *) {
            fishNode.physicsBody?.continuousCollisionDetectionThreshold = 0.04
        }
        sceneView.scene.rootNode.addChildNode(fishNode)
    }
    
    func createARPlaneNode(planeAnchor: ARPlaneAnchor, color: UIColor) -> SCNNode {
        let planeGeometry = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
        let planeMaterial = SCNMaterial()
        planeMaterial.diffuse.contents = "art.scnassets/Surface_DIFFUSE.png"
        planeGeometry.materials = [planeMaterial]
        
        let planeNode = SCNNode(geometry: planeGeometry)
        planeNode.position = SCNVector3Make(planeAnchor.center.x, -0.5, planeAnchor.center.z)
        planeNode.transform = SCNMatrix4MakeRotation(-Float.pi / 2, 1, 0, 0)
        planeNode.physicsBody = createARPlanePhysics(geometry: planeGeometry)
        return planeNode
    }
    
    func createARPlanePhysics(geometry: SCNGeometry) -> SCNPhysicsBody {
        let physicsBody = SCNPhysicsBody(type: .kinematic, shape: SCNPhysicsShape(geometry: geometry, options: nil))
        physicsBody.restitution = 0.5
        physicsBody.friction = 0.5
        return physicsBody
    }
    
   func hideARPlaneNodes() {
        for anchor in (self.sceneView.session.currentFrame?.anchors)! {
            if let node = self.sceneView.node(for: anchor) {
                for child in node.childNodes {
                    let material = child.geometry?.materials.first!
                    material?.colorBufferWriteMask = []
                }
            }
        }
    }
    
    func suspendARPlaneDetection() {
        let config = sceneView.session.configuration as! ARWorldTrackingConfiguration
        config.planeDetection = .horizontal
        sceneView.session.run(config)
    }
    
    func updateARPlaneNode(planeNode: SCNNode, planeAnchor: ARPlaneAnchor) {
        let planeGeometry = planeNode.geometry as! SCNPlane
        planeGeometry.width = CGFloat(planeAnchor.extent.x)
        planeGeometry.height = CGFloat(planeAnchor.extent.z)
        planeNode.position = SCNVector3Make(planeAnchor.center.x, 0, planeAnchor.center.z)
        planeNode.physicsBody = nil
        planeNode.physicsBody = createARPlanePhysics(geometry: planeGeometry)
    }
    
    func updateStatus() {
        statusLabel.text = trackingStatus
    }
    
    func updateFocusNode() {
        focusPoint = CGPoint(x: view.center.x, y: view.center.y)
        let results = sceneView.hitTest(focusPoint, types: [.existingPlaneUsingExtent])
        if results.count == 1 {
            if let match = results.first {
                let t = match.worldTransform
                focusNode.position = SCNVector3(x: t.columns.3.x,
                                                y: t.columns.3.y + 0.1,
                                                z: t.columns.3.z)
            }
        }
    }
    
    func updateFishNodes() {
        for node in sceneView.scene.rootNode.childNodes {
            if node.name == "Fish" {
                if node.presentation.position.y < -2 {
                    node.removeFromParentNode()
                    fishCount += 1
                }
            }
        }
    }
    
    // MARK: Lighting
    func insertSpotLight(position: SCNVector3) {
        let spotLight = SCNLight()
        spotLight.type = .spot
        
        let spotNode = SCNNode()
        spotNode.name = "SpotNode"
        spotNode.light = spotLight
        spotNode.position = position
        
        spotNode.eulerAngles = SCNVector3(-Double.pi / 2, 0, -0.2)
        sceneView.scene.rootNode.addChildNode(spotNode)
    }
    
    func insertOmniLight(position: SCNVector3) {
        let omniLight = SCNLight()
        omniLight.type = .omni
        
        let omniNode = SCNNode()
        omniNode.name = "OmniNode"
        omniNode.light = omniLight
        omniNode.position = position
        
        omniNode.eulerAngles = SCNVector3(-Double.pi / 2, 0, -0.2)
        sceneView.scene.rootNode.addChildNode(omniNode)
    }
    
    func insertAreaLight(position: SCNVector3) {
        let areaLight = SCNLight()
        areaLight.type = .area
        
        let areaLightNode = SCNNode()
        areaLightNode.name = "AreaLightNode"
        areaLightNode.light = areaLight
        areaLightNode.position = position
        
        areaLightNode.eulerAngles = SCNVector3(-Double.pi / 2, 0, -0.2)
        sceneView.scene.rootNode.addChildNode(areaLightNode)
    }
    
    func insertAmbientLight(position: SCNVector3) {
        let ambientLight = SCNLight()
        ambientLight.type = .ambient
        
        let ambientLightNode = SCNNode()
        ambientLightNode.name = "AmbientLightNode"
        ambientLightNode.light = ambientLight
        ambientLightNode.position = position
        
        ambientLightNode.eulerAngles = SCNVector3(-Double.pi / 2, 0, -0.2)
        sceneView.scene.rootNode.addChildNode(ambientLightNode)
    }
    
    func insertIESLight(position: SCNVector3) {
        let iesLight = SCNLight()
        iesLight.type = .IES
        
        let iesLightNode = SCNNode()
        iesLightNode.name = "IESLightNode"
        iesLightNode.light = iesLight
        iesLightNode.position = position
        
        iesLightNode.eulerAngles = SCNVector3(-Double.pi / 2, 0, -0.2)
        sceneView.scene.rootNode.addChildNode(iesLightNode)
    }
    
    func insertDirectionalLight(position: SCNVector3) {
        let directionalLight = SCNLight()
        directionalLight.type = .directional
        
        // Add shadow
        directionalLight.castsShadow = true
        directionalLight.automaticallyAdjustsShadowProjection = true
        directionalLight.maximumShadowDistance = 20.0
        directionalLight.orthographicScale = 1
        directionalLight.shadowMapSize = CGSize(width: 2048, height: 2048)
        directionalLight.shadowMode = .deferred
        directionalLight.shadowSampleCount = 128
        directionalLight.shadowRadius = 3
        directionalLight.shadowBias = 32
        directionalLight.zNear = 1
        directionalLight.zFar = 1000
        directionalLight.shadowColor = UIColor.black.withAlphaComponent(0.36)
        
        let directionalLightNode = SCNNode()
        directionalLightNode.name = "DirectionalNode"
        directionalLightNode.light = directionalLight
        directionalLightNode.position = position
        
        directionalLightNode.eulerAngles = SCNVector3(-Double.pi / 2, 0, -0.2)
        sceneView.scene.rootNode.addChildNode(directionalLightNode)
    }
    

    // MARK: Gestures
    
    func setupSwipeGestures() -> [UISwipeGestureRecognizer] {
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes))
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes))

        swipeUp.direction = .up
        swipeDown.direction = .down

        view.addGestureRecognizer(swipeUp)
        view.addGestureRecognizer(swipeDown)

        return [swipeUp, swipeDown]
    }

    func setupPanGestures(swipeGestures: [UISwipeGestureRecognizer]) {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handleThePan))
        for swipeGeature in swipeGestures {
            panGesture.require(toFail: swipeGeature)
        }
        view.addGestureRecognizer(panGesture)
    }
    
    func setupPinchGesture()  {
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(handlePinch))
        view.addGestureRecognizer(pinch)
    }
    
    func setupTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        view.addGestureRecognizer(tapGesture)
    }
    
    func setupLongPressGesture() {
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        view.addGestureRecognizer(longPress)
    }
    
    @objc func handleSwipes() {
        print("swipe")
        if isStarted {
            guard let frame = self.sceneView.session.currentFrame else { return }
            throwFishNode(transform: SCNMatrix4(frame.camera.transform), offset: fishOffset)
            removeFocusNode()
            sceneView.autoenablesDefaultLighting = false
            isStarted = false
        }
    }
    
    @objc func handleThePan(_ gesture: UIPanGestureRecognizer) {
        print("pan")
        guard fishNode != nil else { return }
        let translation = gesture.translation(in: gesture.view)
        var newAngleY = (Float)(translation.x)*(Float)(Double.pi)/180.0
        
        newAngleY += currentAngleY
        fishNode?.eulerAngles.y = newAngleY
        
        if gesture.state == .ended{
            currentAngleY = newAngleY
        }
    }
    
    @objc func handlePinch(_ gesture: UIPinchGestureRecognizer) {
         print("pinch")
         guard fishNode != nil else { return }
         var originalScale = fishNode?.scale
         switch gesture.state {
         case .began:
             originalScale = fishNode?.scale
             gesture.scale = CGFloat((fishNode?.scale.x)!)
         case .changed:
             guard var newScale = originalScale else { return }
             if gesture.scale < 0.01 {
                 newScale = SCNVector3(x: 0.01 , y: 0.01, z: 0.01 )
             } else if gesture.scale > 0.1 {
                 newScale = SCNVector3(x: 0.1, y: 0.1, z: 0.1)
             } else {
                 newScale = SCNVector3(gesture.scale, gesture.scale, gesture.scale)
             }
             fishNode?.scale = newScale
         case .ended:
             guard var newScale = originalScale else { return }
             if gesture.scale < 0.01 {
                 newScale = SCNVector3(x: 0.01 , y: 0.01, z: 0.01 )
             } else if gesture.scale > 0.1 {
                 newScale = SCNVector3(x: 0.1, y: 0.1, z: 0.1)
             } else {
                 newScale = SCNVector3(gesture.scale, gesture.scale, gesture.scale)
             }
             fishNode?.scale = newScale
             gesture.scale = CGFloat((fishNode?.scale.x)!)
         default:
             gesture.scale = 0.05
             originalScale = nil
         }
     }
    
    @objc func handleTap(_ recognizer: UITapGestureRecognizer) {
        print("tap")
        if isStarted {
            let hisTestResult = sceneView.hitTest(focusPoint, types: .existingPlane)
            if let hitTest = hisTestResult.first {
                setupFishNode()
                let position = SCNVector3(hitTest.worldTransform.columns.3.x,
                                          hitTest.worldTransform.columns.3.y + 0.05,
                                          hitTest.worldTransform.columns.3.z)
                let positionLight =  SCNVector3(hitTest.worldTransform.columns.3.x,
                                                hitTest.worldTransform.columns.3.y + 0.3,
                                                hitTest.worldTransform.columns.3.z)
                fishNode.position = position
                fishNode.physicsBody = .static()
                insertDirectionalLight(position: positionLight)
//                insertOmniLight(position: positionLight)
//                insertIESLight(position: positionLight)
//                insertAreaLight(position: positionLight)
//                insertSpotLight(position: positionLight)
//                insertAmbientLight(position: positionLight)
                sceneView.scene.rootNode.addChildNode(fishNode)
                removeFocusNode()
                isStarted = false
            }
        }
    }
   
    @objc func handleLongPress(_ recognizer: UILongPressGestureRecognizer) {
        print("long press")
        let touch = recognizer.location(in: sceneView)
        let hitTestResults = self.sceneView.hitTest(touch, options: nil)

        if let _ = hitTestResults.first {
                if recognizer.state == .began {
                    localTranslatePosition = touch
                } else if recognizer.state == .changed {

                    let deltaX = Float(touch.x - self.localTranslatePosition.x)/700
                    let deltaY = Float(touch.y - self.localTranslatePosition.y)/700

                    fishNode.localTranslate(by: SCNVector3(deltaX,0.0,deltaY))
                    self.localTranslatePosition = touch

                }

        }
    }

    @IBAction func startButtonPressed(_ sender: Any) {
        start()
    }
    
    @IBAction func resetButtonPressed(_ sender: Any) {
        reset()
    }
    
    func start() {
        DispatchQueue.main.async {
            self.startButton.isHidden = true
            self.suspendARPlaneDetection()
            self.hideARPlaneNodes()
            self.isStarted = true
        }
    }
    
    func resetARSession() {
        let config = sceneView.session.configuration as! ARWorldTrackingConfiguration
        config.planeDetection = .horizontal
        let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
        sceneView.session.run(config, options: options)
    }
    
    func resetNode() {
        for nodes in sceneView.scene.rootNode.childNodes {
            nodes.removeFromParentNode()
        }
        
        for anchor in (self.sceneView.session.currentFrame?.anchors)! {
            if let node = self.sceneView.node(for: anchor) {
                for child in node.childNodes {
                    child.removeFromParentNode()
                }
            }
        }
    }
    
    func removeFocusNode() {
        if focusNode != nil {
            focusNode.removeFromParentNode()
        }
    }
    
    func reset() {
        DispatchQueue.main.async {
            self.startButton.isHidden = false
            self.resetARSession()
            self.resetNode()
            self.isReseted = true
            self.isStarted = false
            self.loadModels()
        }
    }

    // MARK: - ARSCNViewDelegate
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor, !isStarted else { return }
        DispatchQueue.main.async {
            let planeNode = self.createARPlaneNode(planeAnchor: planeAnchor, color: UIColor.yellow.withAlphaComponent(0.5))
            node.addChildNode(planeNode)
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor, !isStarted else { return }
        DispatchQueue.main.async {
            self.updateARPlaneNode(planeNode: node.childNodes[0], planeAnchor: planeAnchor)
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        DispatchQueue.main.async {
            self.updateStatus()
            self.updateFocusNode()
            self.updateFishNodes()
        }
    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        switch camera.trackingState {
        case .notAvailable:
            trackingStatus = "Tracking: Not available!"
        case .normal:
            trackingStatus = "Tracking: All good!"
        case .limited(let reason):
            switch reason {
            case .excessiveMotion:
                trackingStatus = "Tracking: Limited due to excessive motion!"
            case .insufficientFeatures:
                trackingStatus = "Tracking: Limited due to insufficient features!"
            case .initializing:
                trackingStatus = "Tracking: Initializing..."
            case .relocalizing:
                trackingStatus = "Tracking: Relocalizing..."
            @unknown default:
                fatalError()
            }
        }
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        trackingStatus = "AR Session Failure: \(error)"
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        trackingStatus = "AR Session Was Interrupted!"
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
    }
}

// MARK: Double Extension

public extension Double {

    /// Returns a random floating point number between 0.0 and 1.0, inclusive.
    static var random: Double {
        return Double(arc4random()) / 0xFFFFFFFF
    }

    /// Random double between 0 and n-1.
    ///
    /// - Parameter n:  Interval max
    /// - Returns:      Returns a random double point number between 0 and n max
    static func random(min: Double, max: Double) -> Double {
        return Double.random * (max - min) + min
    }
}
